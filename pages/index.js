import 'semantic-ui-css/semantic.min.css'
import React from 'react'
import Layout from '../components/Layout'
import { Modal, Header, Button, List, Icon } from 'semantic-ui-react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';

import Home from './home'
import Edit from './edit'
import Create from './create'
import Show from './show'
import About from './about'

class Index extends React.Component {
	render() {
		return (
			<Layout>
				<Router>
					<Route exact path="/" component={Home} />
					<Route exact path="/edit/:id" component={Edit} />
					<Route exact path="/create" component={Create} />
					<Route exact path="/show/:id" component={Show} />
					<Route exact path="/about" component={About} />
				</Router>
			</Layout>
		)
	}
}
export default Index
import Document, { Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render () {
    return (
      <html>
        <Head>
          <link rel='stylesheet' href='/_next/staticc/style.css' />
          <link href="https://fonts.googleapis.com/ss?family= +One|Roboto:300,400" rel="stylesheet"/>
          <link rel="stylesheet" href="/../styles/main.css"/>
          <link href="https://fonts.googleapis.com/css?family=Scope+One" rel="stylesheet"/>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}

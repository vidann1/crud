const express = require('express');
const next = require('next');
const serveStatic = require("serve-static");
const { join } = require("path");

import routes from './routes'

const dev = process.env.NODE_ENV !== 'production'
const handle = app.getRequestHandler()
const { renderComponent } = require('next/server')
const app = next({ dev })

app.prepare()
.then(() => {
  const server = express()
  
  server.get('/show/:id', (req, res) => {
    return app.render(req, res, '/show', { id: req.params.id })
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(3000, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:3000')
  })
})
.catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})










/*
app.prepare().then(() => {
  const server = express();

  server.use(
    serveStatic(join(__dirname, "static"), {
      
    })
  );

  server.get("/show/*", (req, res) => {
    app.render(req, res, "/show", {
      fullUrl: req.originalUrl
    });
  });

  server.use(handler);

  server.listen(port, err => {
    if (err) {
      throw err;
    }
    console.log("> Ready on http://localhost:3000");
  });
});
*/


